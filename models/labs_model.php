<?php
class Labs_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetAllLabsQueue()
    {
        //Call file test json
        // header("refresh: 30; url = http://localhost/hosq/"); 
        // $data = file_get_contents('https://his01.psu.ac.th/hg/ChkO36Qi.php');
        //Decode JSON
        // $json = json_decode($data,false);
        // $json = json_encode($data);
        // echo $json;
        
        // $data = file_get_contents('./public/files/labqueue.json');
        // echo $data;


        //Call From server
        $url ="https://his01.psu.ac.th/hg/ChkO36Qk.php";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $res = iconv("Windows-874", "UTF-8", $result);
        $json = json_decode($res, false);

        echo $res;
    }

    //************** From Controllers **************//
    
    //********************************************************************************//
}
