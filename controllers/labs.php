<?php
class Labs extends Controller
{
    public function __construct()
    {
        parent::__construct('labs');
        // echo 'Inside notfound.php controller.';
    }

    public function index()
    {
        $this->views->msg = 'This page does not exists.';
        $this->views->render('labs/index');
    }

}
