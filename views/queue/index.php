<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="public/src/images/queue.png">
    <link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"> -->

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/b7a24e7c0e.js" crossorigin="anonymous"></script>

    <style>
        body{
            /* background:#1abc9c; */
            overflow-y: hidden;
            font-family:niramit;
            background-image: url('public/src/images/blackboard.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }

        @font-face {
            font-family: niramit;
            src: url(public/fonts/niramit/TH-Niramit-AS.ttf);
        }

        @font-face {
            font-family: niramit-bold;
            src: url(public/fonts/niramit/TH-Niramit-AS-Bold.ttf);
        }

        .font-bold{
            font-family: niramit-bold;
        }

        .text {
            display: block;
            width: 120px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .clinicname{
            font-family: niramit-bold;
            position: fixed; 
            width: 100%;
        }

        .clock{
            font-family: niramit-bold;
            position: fixed;
            right: 0;
            top: 0;
            /* background: rgba(0, 0, 0, 0.5); */
            /* color: #f1f1f1; */
            width: 20%;
            font-size: 48px;
        }

    </style>
    <title>HOSQ</title>
</head>
    <body>
        <section class="content-header mt-3">
            <div class="row ml-5 mr-5">
                <!-- <div class="col-12 text-right">
                    <p id="digitaltime" class="text-white badge badge-pill badge-danger" onload="startTime()" style="font-family:Sarabun; font-size: 32px;">00:00:00</p>
                </div> -->
                <div class="col text-center">
                    <h1 id="clinicname" style="color:white;">Clinic</h1>
                </div>

                <div class="clock text-right mr-5">
                    <p id="digitaltime" class="text-white badge badge-pill badge-danger" onload="startTime()">00:00:00</p>
                </div>
                
            </div>
        </section>

        <!-- <section class="content d-flex justify-content-center" id="sectionlist"> -->
        <section class="content" id="sectionlist">
            <!-- //Version 2 -->
            <div id="queuelist" class="row ml-5 mr-5 d-flex justify-content-center">

            </div>
            <!-- <br> -->
            <!-- <h4 class="text-center text-white" style="font-size: 42px;"><?php date_default_timezone_set("Asia/Bangkok"); echo $date = date('l dS F Y');?></h4> -->
        </section>

        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/axios/dist/axios.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script>
            function refreshTime() {
                // {timeZone: "Asia/Bangkok", hour12: false}
                var options = { timeZone: 'Asia/Bangkok', hour12: false };
                var dateString = new Date().toLocaleTimeString("en-US", options);
                var formattedString = dateString.replace(", ", " - ");
                // timeDisplay.textContent = formattedString;
                $('#digitaltime').text(formattedString);
            }
            setInterval(refreshTime, 1000);
            getCounterName();
            listAllQueue();


            function getCounterName() {
                let counter = "<?php echo $_SESSION["counter"]?>";
                let body = {
                    counter: counter
                }
                let result =  axios
                    .post("http://61.19.201.20:19539/hosq/counter", JSON.stringify(body), {
                        headers: {
                            "Content-Type": "application/json",
                        },
                    })
                    .then((response) => {
                        return response.data;
                    })
                    .catch((err) => {
                        console.error(err);
                    });

                result.then(promres => {
                    $("#clinicname").text(promres.data);
                })
            }

            function listAllQueue() {
                let counter = "<?php echo $_SESSION["counter"]?>";
                let tv = "<?php echo $_SESSION["tv"]?>";
                let body = {
                    counter: counter,
                    tvnumber:tv
                }
                let result = axios
                            .post("http://61.19.201.20:19539/hosq/allqueue", JSON.stringify(body), {
                                headers: {
                                    "Content-Type": "application/json",
                                },
                                })
                            .then((response) => {
                                return response.data;
                            })
                            .catch((err) => {
                                console.error(err);
                            });
                result.then(promres => {
                    let doctorData = promres.data;
                    // console.log(doctorData);
                    $('#queuelist').html("");
                    
                    console.log("ทีวีหมายเลข: " + tv);
                    console.log("จำนวนห้องตรวจทั้งหมด: " + doctorData.length + " ห้อง");
                    doctorData.forEach((doctorelement, roomindex) => {
                        let room = doctorelement.room;
                        let doctName = doctorelement.doctname;
                        let patientsData = doctorelement.patients;

                        if (doctName == "") {
                            doctName = "-"
                        }
                        if (tv == 1) {
                            // console.log("ห้องตรวจที่ 1-6");
                            if (roomindex < 8) {
                                GetPatients(room, doctName, patientsData)
                            }
                        } else if (tv == 2) {
                            // console.log("ห้องตรวจที่ 7-12");
                            // console.log("แสดงผลบน tv หมายเลข 2");
                            if (roomindex >= 8 && roomindex < 12) {
                                GetPatients(room, doctName, patientsData)
                            }
                        }
                       
                    });
                })
            }

            function GetPatients(room, doctName, patientsData){
                let html_doctor_str = "";
                let html_patient_str = "";
                patientsData.forEach((patientelement,patientindex) => {
                    let patient_qtime = patientelement.time;
                    let patient_hn = patientelement.hn;
                    let patient_name = patientelement.name;

                    if (patientindex< 5) {
                        //ใส่ dot ใน HN 3 ตัวแรก
                        let dotString = "";
                        for (let k = 0; k < 3; k++) {
                            dotString +=
                            "<i class='fa fa-circle align-middle' style='font-size:8px'></i>";
                        }
                        let new_patient_hn = dotString + patient_hn.substring(3);
                        let qTime = "";

                        if (patientindex == 0) {
                            qTime =
                                `<span class='badge badge-pill badge-success' style='font-size: 22px;'>
                                    ${patient_qtime} น.
                                </span>`;
                        } else{
                            qTime =
                                `<span class='badge badge-pill badge-warning' style='font-size: 22px;'>
                                    ${patient_qtime} น.
                                </span>`;
                        }
                        html_patient_str +=
                        `<div class='row d-flex justify-content-center' style="width:100%; margin-top:-18px; margin-left:0px; margin-right:0px; margin-bottom:-12px;">
                                <div class='col-auto mt-2'style="padding-right:8px; padding-left:8px;" >
                                    ${qTime}
                                </div>
                                <div class='col-auto mt-1' style="padding-right:0px; padding-left:0px;">
                                    <span style='font-size: 28px;'>
                                        ${new_patient_hn}
                                    </span>
                                </div>
                                <div class='col-auto' style="padding-right:8px; padding-left:8px;">
                                    <span class="text font-bold text-right" style='font-size:32px;'>
                                        ${patient_name}
                                    </span>
                                </div>
                        </div>
                        <hr class="ml-2 mr-2" style='border-top:1px solid orange; border-radius: 5px; height: 1px;'>`;
                    }
                });
                html_doctor_str =
                `<div class='col-md-3 col-sm-6 col-xs-12 mt-3'>
                        <div class='h-100 bg-white pt-5' style="border-radius:5%;">
                            <div class='row'>
                                <div class="col-12" style="margin-top:-55px; margin-left:16px;">
                                    <img class="float-left" src="public/src/images/pin.png" alt="note pin" width="40px" height="55px">
                                </div>
                                <div class='col-12 text-center' style="margin-top:-45px;">
                                    <span class="font-bold" style='font-size:28px;'>
                                        ห้องตรวจ ${room}
                                    </span>
                                </div>
                                <div class='col-12 text-center'>
                                    <span style='font-size: 22px'>
                                        <i class='fa fa-user-md text-info'></i>&nbsp
                                        แพทย์: 
                                        ${doctName}
                                    </span><br/>
                                    <hr style='border-top:2px solid #eee; border-radius: 5px; height: 1px; margin-top: -2px;'>
                                </div>
                                <div class="col-12">${html_patient_str}</div>
                            </div>
                        </div>
                    </div>`;
            
                // $("#queuelist").append(html_doctor_str).fadeIn();
                $("#queuelist").append(html_doctor_str);
                html_patient_str = "";
            }

            setInterval(function(){
                // $('#queuelist').fadeOut("slow", function(){
                //     // location.reload();
                //     listAllQueue();
                // });
                // $('#queuelist').fadeOut("slow");
                listAllQueue();
                console.log("Reload Queue");
            }, 60000);
        </script>
    </body>
</html>
