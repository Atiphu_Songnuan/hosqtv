<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Under Maintenance</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

	<style>
		.page-wrap {
			min-height: 100vh;
		}

		h3{
			font-family: 'Prompt', sans-serif;
		}

		h4{
			font-family: 'Prompt', sans-serif;
		}

		h2{
			font-family: 'Prompt', sans-serif;
		}
	</style>
</head>

<body>
	<!-- <div class="d-flex justify-content-center">
		<h3>ขออภัยในความไม่สะดวก</h3>
		<img src="public/src/img/maintenance.svg" width="1024" height="512" alt="maintenance">
		<h2 style="font-size: 50px; font-weight: 100%; color: #262626;"><span>อยู่ระหว่างปรับปรุงระบบ</span></h2>
		<h2>ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์ <br/> มหาวิทยาลัยสงขลานครินทร์</h2>
	</div> -->

	<!-- <div class="d-flex flex-row align-items-center">
		<h3>ขออภัยในความไม่สะดวก</h3>
	</div> -->

	<div class="page-wrap d-flex flex-row align-items-center">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 text-center">
					<h3 style="font-family: 'Prompt', sans-serif;">ไม่พบ PATH ที่ระบุ</h3>
					<img src="public/src/images/maintenance.svg" width="800" height="256" alt="maintenance" style="margin-top:48px; margin-bottom:16px;">
					<h2 style="font-size: 50px; font-weight: 100%; color: #262626; margin-bottom:48px; color:tomato;"><span>กรุณาระบุ PATH ที่ถูกต้อง</span></h2>
					<h4>ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์ <br/> มหาวิทยาลัยสงขลานครินทร์</h4>
				</div>
			</div>
		</div>
	</div>

	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script>
		 setInterval(function () {
            location.reload();
			console.log("Refresh Page");
        }, 65000);
	</script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
