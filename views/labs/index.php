<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- <link rel="stylesheet" href="./public/vendor/bootstrap/dist/css/bootstrap.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <!-- <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Sarabun:700&display=swap" rel="stylesheet">
    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="./public/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./public/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./public/dist/css/skins/_all-skins.min.css">

    <style>
        body{
            background:#1abc9c;
            /* overflow-y: hidden; */
        }

        span{
            font-family:"Sarabun";
            
        }

        .info-box{
            border-radius: .25rem; 
            box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
        }

        .info-box-icon{
            box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
        }

    </style>
    <title>HOSQ</title>
</head>
<body>
    <section class="content-header mt-3">
        <div class="row ml-5 mr-5">
            <!-- <div class="col-6">
                <h4 class="text-center text-white" ><?php date_default_timezone_set("Asia/Bangkok"); echo $date = date('l dS F Y'); ?></h4>
            </div> -->
            <!-- <div class="col">
                <h1 class="text-white text-center ml-5" style="font-family:Sarabun; font-size: 56px;">
                    คิวตรวจ OPD ER
                </h1>
            </div> -->
            <div class="col-12 text-center mt-3">
                <p id="digitaltime" class="text-white badge badge-pill badge-danger" onload="startTime()" style="font-family:Sarabun; font-size: 80px;">00:00:00</p>
            </div>  
        </div>         
    </section>

    <section class="content" id="sectionlist">
        
        <div id="labsqueuelist" class="row ml-5 mr-5">
           
        </div>
        <br>
    </section>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="./public/js/jquery.js"></script>

    <script>
        function refreshTime() {
            // {timeZone: "Asia/Bangkok", hour12: false}
            var options = { timeZone: 'Asia/Bangkok', hour12: false };
            var dateString = new Date().toLocaleTimeString("en-US", options);
            var formattedString = dateString.replace(", ", " - ");
            // timeDisplay.textContent = formattedString;
            $('#digitaltime').text(formattedString);
        }
        setInterval(refreshTime, 1000);

        listAllLabsQueue();
        function listAllLabsQueue(){
            var newtab = false;
                $('#labsqueuelist').html("");
                $.ajax({
                    type:"GET",
                    url:"../hosq/ApiService/GetAllLabsQueue",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    dataType: "json",
                    success: function(response) {
                        let html_doctor_str = "";
                        let html_patient_str = "";
                        console.log(response);
                        for (let i = 0; i < response.STAFF.doctors.length; i++) {
                            const element = response.STAFF.doctors[i];
                            // console.log(element);

                            let doc_name = element.DName;
                            let room = element.Room;
                            let patients = element.patient;
                            // console.log(doc_name + " Room: " + room);
                            if (i < 8) {
                                for (let j = 0; j < patients.length; j++) {
                                    let patient_pwait = patients[j].pwait;
                                    let patient_hn = patients[j].hn;
                                    let patient_name = patients[j].pname;

                                    // console.log("Patient Queue: " + patient_name);
                                    if (j < 5) {

                                        //ใส่ dot ใน HN 3 ตัวแรก
                                        let dotString = "";
                                        for (let k=0; k < 3; k++) { 
                                            dotString += "<i class='fa fa-circle align-middle' style='font-size:12px'></i>";
                                        }
                                        let new_patient_hn = dotString + patient_hn.substring(3);

                                        let pwait = "";
                                        pwait = "<span class='waiting badge badge-pill badge-warning' style='font-size: 40px;'>"+patient_pwait+"</span>";
                                       
                                        html_patient_str += "<div class='row'>" + 
                                                                "<div class='col-auto mt-3'>"+
                                                                pwait +
                                                                "</div>"+
                                                                "<div class='col-auto ml-1'>"+
                                                                    "<span style='font-size: 58px;'>"+new_patient_hn+"</span>"+
                                                                "</div>"+
                                                                "<div class='col-auto mr-3'>"+
                                                                    "<span class='float-left' style='font-size: 58px;'>"+patient_name+"</span>"+
                                                                "</div>"+
                                                            "</div>"+
                                                            "<hr style='border-top:1px solid orange; border-radius: 5px; height: 1px;'>";
                                    }
                                    html_doctor_str = "<div class='col-md-4 col-sm-6 col-xs-12 mt-5'>"+
                                                        "<div class='info-box h-100'>"+
                                                            "<div class='row'>"+
                                                                "<div class='col-12'>"+
                                                                    "<span class='info-box-icon bg-yellow w-100'><span style='font-size:56px'>"+ room +"</span></span>"+
                                                                "</div>"+
                                                                "<div class='col-12'>"+
                                                                    "<div class='ml-5 mr-5'>" +
                                                                        // "<span class='info-box-icon bg-yellow w-100'><span style='font-size:46px'>"+ room +"</span></span>"+
                                                                        "<span class='info-box-text mt-4' style='font-size: 40px'><i class='fa fa-user-md text-danger'></i>&nbsp"+doc_name+"</span><br/>"+
                                                                        html_patient_str+
                                                                        "</span>" +
                                                                    "</div>"+
                                                                "</div>"+
                                                            "</div>"+
                                                        "</div>"+
                                                    "</div>";
                                }
                                $('#labsqueuelist').append(html_doctor_str).fadeIn();
                                html_patient_str = "";
                            } 
                            // else if (i > 5 && i < 12) {

                            //     console.log("ห้องตรวจมากกว่า 6 ห้อง");

                            // }
                        }
                    },
                    error: function(response) {
                        console.log(response);
                    }
                });
        }

        setInterval(function(){
            $('#labsqueuelist').fadeOut("slow", function(){
                // location.reload();
                listAllLabsQueue();
            });
        }, 30000);

        // setInterval(function(){
        //     $('.waiting').fadeOut().fadeIn();
        // }, 1500);

    </script>
</body>
</html>