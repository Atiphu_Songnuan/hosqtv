<?php

// Start session for login
session_start();
class Router
{
    public function __construct()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
  
        $counter = $_GET["counter"];
        $tv = $_GET["tv"];
        if (!empty($counter) && !empty($tv)) {
            $_SESSION["counter"] = $counter;
            $_SESSION["tv"] = $tv;
        }
        require_once './controllers/queuelistall.php';
        $controller = new QueueListAll();
        $controller->index();
        return false;

        // *********************************************************************************************
        // // กรณีที่ไม่มี parameter จะ default ไปหน้า main
        // if (empty($url[0])) {
        //     require_once './controllers/queuelistall.php';
        //     $controller = new QueueListAll();
        //     $controller->index();
        //     return false;
        // } else {
        //     $_SESSION["cliniccode"] = $_GET['clinic'];
        //     $_SESSION["tvnumber"] = $_GET['tv'];
        //     require_once './controllers/queuelistall.php';
        //     $controller = new QueueListAll();
        //     $controller->index();
        //     return false;
        //     // $this->notfound();
        //     // หากมี parameter จะต้องระบุ 2 ค่า คือ Clinic-Code, TV Number
        //     // if (count($url) == "2") {
        //     //     // $_SESSION["cliniccode"] = $url[0];
        //     //     // $_SESSION["tvnumber"] = $url[1];

        //     //     // header( "location: /hosqtv?cliniccode=".$url[0]. "&tvnumber=".$url[1]);
        //     //     // print_r("<br>" . "Clinic Code: " . $_SESSION["cliniccode"] . "<br>");
        //     //     // print_r("TV Number: " . $_SESSION["tvnumber"]);
        //     //     // $cliniccode = $url[0];
        //     //     // $tvnumber = $url[1];
        //     //     // print_r($url);
        //     //     require_once './controllers/queuelistall.php';
        //     //     $controller = new QueueListAll();
        //     //     $controller->index();
        //     //     return false;

        //     // } else {
        //     //     // echo "ไม่พบ path ที่ระบุ";
        //     //     $this->notfound();
        //     // }
        // }
        // *********************************************************************************************

        // $file = "<br>" . './controllers/' . $url[0] . '.php';
        // $fileApi = "<br>" . './libs/' . $url[0] . '.php';

        // print_r($file);
        // print_r($fileApi);
        // //ตรวจสอบไฟล์ใน folder controllers ว่ามีหรือไม่มี ถ้าไม่มีให้เรียก notfound.php ขึ้นมาแสดง
        // if (file_exists($file)) {
        //     require_once $file;
        // } else if (file_exists($fileApi)) {
        //     require_once $fileApi;
        // } else {
        //     $this->error();
        //     // require_once 'controllers/notfound.php';
        //     // $controller = new NotFound();
        //     // return false;
        //     // throw new Exception("The File: $file does not exists!");
        // }

        // $controller = new $url[0];
        // $controller->loadModel($url[0]);
        // // session_start();
        // //ตรวจสอบข้อมูลที่ส่งผ่านมาทาง url ว่ามีการเข้าถึง method ไหน หรือมีการส่งค่าอะไรเข้าไปใน method บ้าง
        // // if (isset($url[1])) {
        // //     if (isset($url[2])) {
        // //         $controller->{$url[1]}($url[2]);
        // //         // return false;
        // //     } else {
        // //         $controller->{$url[1]}();
        // //         // return false;
        // //     }
        // // }

        // //Calling methods
        // if (isset($url[2])) {
        //     if (method_exists($controller, $url[1])) {
        //         $controller->{$url[1]}($url[2]);
        //     } else {
        //         // echo 'Errrrrrrr';
        //         $this->error();
        //     }
        // } else {
        //     if (isset($url[1])) {
        //         if (method_exists($controller, $url[1])) {
        //             $controller->{$url[1]}();
        //         } else {
        //             $this->error();
        //         }

        //     } else {
        //         $controller->index();
        //     }
        // }

    }

    public function notfound()
    {
        require_once './controllers/notfound.php';
        $controller = new NotFound();
        $controller->index();
        return false;
    }

    public function error()
    {
        require_once './controllers/error.php';
        $controller = new Error();
        $controller->index();
        return false;
    }
}
