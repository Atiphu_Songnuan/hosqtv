<?php
class View
{
    public function __construct()
    {
        // echo '***************************<br/>';
        // echo 'Inside view.php Libs.<br/>';
        // echo '***************************<br/>';
    }

    public function render($name)
    {
        require_once 'views/' . $name . '.php';
    }
}
