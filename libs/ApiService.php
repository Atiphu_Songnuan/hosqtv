<?php
class ApiService
{
    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }

    public function GetAllQueue()
    {
        $this->loadModel("queuelistall");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->mainID;
        // $mID = $request->mainID;
        $this->model->GetAllQueue();
    }

    public function GetAllLabsQueue()
    {
        $this->loadModel("labs");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->mainID;
        // $mID = $request->mainID;
        $this->model->GetAllLabsQueue();
    }


}
